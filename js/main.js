
/* Récupération des différentes zones de la page*/
const contact_list = document.querySelector("#contact_list");
const messages_list = document.querySelector("#messages_list");
const message_box = document.querySelector("#message_box");

/* Instanciation de la classe en charge d'afficher les différentes parties*/
const app = new App(contact_list, messages_list, message_box);

/* Ajout des bots à la liste de dontacts*/
app.addBot(new R2D2());
app.addBot(new C3PO());
app.addBot(new BB8());
