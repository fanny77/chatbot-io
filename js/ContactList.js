/**
 * Classe en charge de la gestion de l'UI liste de contact
 */
class ContactList
{
	constructor(contact_list_element)
	{
		this.root = contact_list_element;
	}

	/* Création d'un contact dans la liste de contacts*/
	add(name, description)
	{
		const contact_div =  document.createElement("div");
		contact_div.id = "bot_" + name.replace(' ','_');
		contact_div.classList.add("contact");

		const avatar_i = document.createElement("i");
		avatar_i.classList.add("fas", "fa-user-circle", "avatar");

		const div = document.createElement("div");
		const name_div = document.createElement("div");
		const desc_div = document.createElement("div");

		name_div.classList.add("name");
		name_div.innerText = name;

		desc_div.classList.add("description");
		desc_div.innerText = description;

		div.appendChild(name_div);
		div.appendChild(desc_div);

		contact_div.appendChild(avatar_i);
		contact_div.appendChild(div);

		this.root.appendChild(contact_div);
	}

}
