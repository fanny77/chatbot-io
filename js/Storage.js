/**
 * Classe en charge du stockage de l'historique des messages
 */
class MessageStorage
{
	static load()
	{
		let messages = [];

		Object.keys(localStorage).sort().forEach((key) => {
			let item = JSON.parse(localStorage.getItem(key));
			item.date = new Date(item.date);
			messages.push(item);
		});

		return messages;
	}

	/* Stockage des messages dans le storage du navigateur */
	static addMessage(msg, date, author)
	{
		/* Détermine la clé du stockage*/
		const key_prefix = date.getTime();
		let key_index = 0;

		while(localStorage.getItem(key_prefix + "-" + key_index) !== null)
			key_index++;

		const item = {
			msg: msg,
			date: date,
			author: author
		}

		/*Stringify = transformer le message(item) en chaîne de caractère JSON*/
		localStorage.setItem(key_prefix + "-" + key_index, JSON.stringify(item));
	}
}
