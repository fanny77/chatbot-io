/**
 * Classe en charge de l'affichage de la conversation
 */
class MessageList
{
	constructor(message_list_element)
	{
		this.root = message_list_element;
	}

	add(msg, datetime, author = null)
	{

		const message_div = document.createElement("div");
		message_div.classList.add("message");
		message_div.classList.add((author) ? "other" : "self");

		const div = document.createElement("div");
		message_div.appendChild(div);

		const content_div = document.createElement("div");
		content_div.classList.add("content");

		/*Vérification auteur*/
		if(author){
			const avatar_i = document.createElement("i");
			avatar_i.classList.add("fas", "fa-user-circle", "avatar");
			div.appendChild(avatar_i);

			const name_span = document.createElement("span");
			name_span.classList.add("contact_name");
			name_span.innerText = author;

			content_div.appendChild(name_span);
		}

		div.appendChild(content_div);
		content_div.insertAdjacentHTML("beforeend", msg);

		/* Affichage de la date*/
		if(datetime instanceof Date){
			const meta_div = document.createElement("div");
			meta_div.classList.add("meta");
			meta_div.innerText = datetime.toLocaleString();

			message_div.appendChild(meta_div);
		}

		this.root.appendChild(message_div);

		/* Gestion du scroll*/
		this.root.scroll({
			top: this.root.scrollHeight,
			left: 0,
			behavior: "smooth"
		});
	}

	clear()
	{
		this.root.innerHTML = "";
	}
}
