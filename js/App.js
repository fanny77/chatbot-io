class App
{
	constructor(contact_list, messages_list, message_box)
	{
		this.contact_list = new ContactList(contact_list);
		this.messages_list = new MessageList(messages_list);

		this.bots = [];

		this._bindMessageBox(message_box);

		/* Load messages */
		MessageStorage.load().forEach((item) => {
			this.messages_list.add(item.msg, item.date, item.author);
		});
	}

	addBot(bot)
	{
		this.bots.push(bot);
		this.contact_list.add(bot.name, bot.description);
	}

	sendMessage(msg, author = null)
	{
		const datetime = new Date();

		this.messages_list.add(msg, datetime, author);
		MessageStorage.addMessage(msg, datetime, author);

		if(author === null){
			this.sendCommand(msg);
		}
	}

	sendCommand(msg)
	{
		const words = msg.split(' ');
		const command = words.shift();
		const params = (words.length) ? words.join(' ') : '';

		for(let i = 0; i < this.bots.length; i++){
			this.bots[i].execute(command, params);
		}
	}

	/** interne */

	_bindMessageBox(message_box)
	{
		const callback = () => {
			const input = message_box.querySelector("#message_input");

			if(input.value === "")
				return;

			this.sendMessage(this.htmlEntities(input.value));
			input.value = "";
		}

		message_box.querySelector("#message_input").addEventListener('keypress', (ev) => {
			if(ev.code == "Enter")
				callback();
		});

		message_box.querySelector("input[type='button']").addEventListener('click', () => {
			callback();
		})
	}

	/** SNIPPETS */
	htmlEntities(str) {
		return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
	}
}
