/**
 *  Classe en charge de récupérer la météo sur l'api OpenWeatherMap
 */
class Weather
{
	constructor()
	{
		this.API_KEY = "872f5825b5a016d75ac6990cc0e8218a";
		this.API_URL = "https://api.openweathermap.org/data/2.5/weather";
	}

	async get(city)
	{
		const url = this.API_URL + "?q=" + city + "&units=metric&lang=fr&appid=" + this.API_KEY;
		const data = await fetch(url).then(response => response.json());

		return data;
	}

	formatMessage(data)
	{
		if(data.cod === "404"){
			return "La ville que vous avez demandée est introuvable !";
		}

		return `Actuellement, le temps à ${data.name} est ${data.weather[0].description}.<br>
				La température est de ${Math.round(data.main.temp)}°C.`;
	}
}
