/**
 *  Classe en charge de récupérer les 10 films les plus populaires sur l'api IMDb
 */
 class IMDb
 {
	constructor()
	{
		this.API_KEY = "k_jw5u73l1";
		this.API_URL = "https://imdb-api.com/fr/API/MostPopularMovies/";
	}

	async get()
	{
		const url = this.API_URL + this.API_KEY;
		const data = await fetch(url).then(response => response.json());

		return data;
	}

	formatMessage(data)
	{
		if (data.errorMessage != "") {
			return "Un problème est survenu, veuillez réessayer plus tard"
		}
		let result = "Voici les 10 films les plus populaires : <br><ul>";
		for (let i = 0; i <= 10; i++) {
			result += "<li>" + data.items[i].title + "</li>";
		}
		return result + "</ul>";
	}
}
