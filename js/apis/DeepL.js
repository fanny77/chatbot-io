/**
 *  Classe en charge de traduire le message de l'utilisateur via l'API DeepL
 */
 class DeepL
 {
	constructor()
	{
		this.API_KEY = "92df21d8-e9a7-9e3b-a2ff-78f302628714:fx";
		this.API_URL = "https://api-free.deepl.com/v2/translate?";
	}

	async get(text, target_lang)
	{
		const url = `${this.API_URL}auth_key=${this.API_KEY}&text=${text}&target_lang=${target_lang}`;
		const data = await fetch(url).then(response => response.json());

		return data;
	}

	formatMessage(data)
	{
		return `La traduction du texte demandé est : ${data.translations[0].text}`;
	}
}
