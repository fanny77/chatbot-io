/**
 * Classe en charge de la création et de la configuration des bots
 */
class Bot
{

	constructor(name, description)
	{
		this.name = name;
		this.description = description;
		this.commands = {};
	}

	registerAction(command, action)
	{
		this.commands[command] = action;
	}

	execute(command, params)
	{
		if(command in this.commands)
		{
			this.commands[command](params, this.name);
		}
	}
}
