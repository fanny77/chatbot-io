class C3PO extends Bot
{

	constructor()
	{
		super("C3PO", "Relation humains-ciborgs !");
		this.registerAction("hello", this.hello.bind(this));
		this.registerAction("traduire", this.traduire.bind(this));
		this.registerAction("calcule",this.calculer.bind(this));
		this.registerAction("help",this.helper.bind(this));

	}

	/* COMMANDES */
	
	hello(params)
	{
		if(params == '')
			app.sendMessage("Bonjour je suis C3PO, robot protocolaire, à votre service !", this.name);
	}

	helper(params)
	{
		if (params == '') {
			app.sendMessage("Mes commandes sont : <ul> <strong> <li> hello : pour vous saluer</li> <li> traduire &lt;LangueDestination&gt; &lt;motATraduire&gt; : Je traduis le mot ou la phrase que vous voulez dans la langue supportés ci dessous : </strong><ul><li> DE - Allemand </li> </li><li> EN - Anglais </li> <li> ES - Espagnol </li> <li> FR- Français </li> <li> IT - Italien </li> <li> PT - Portugais </li> <li> RU - Russe </li> <li> ZH - Chinois </li> </ul> </li> <li><strong>calcule &lt;Opération&gt; : Je calcule ce que vous voulez en temps réel </li> </strong></ul>",this.name)
		}
	}

	traduire(params)
	{
		const requestParams = params.split(' ');

		if(requestParams.length < 2)
		{
			return;
		}

		const target_lang = requestParams.shift();
		const text = requestParams.join(' ');
		const traductor = new DeepL();

		traductor.get(text, target_lang).then(data => {
			app.sendMessage(
				traductor.formatMessage(data),
				this.name
			);
		});
	}

	calculer(params)
	{
		
		if (!params.match(/^[0-9()\ *\/+-]+$/g)) 
		{
			app.sendMessage("Les calculs sont pas bons Kévin !",this.name);
			return;
		}
		
		app.sendMessage("Le résultat est : " + eval(params), this.name);
	};
}
