class BB8 extends Bot
{

	constructor()
	{
		super("BB8", "Le robot le plus rapide de la galaxie !");
		this.registerAction("hello", this.hello.bind(this));
		this.registerAction("films", this.films.bind(this));
		this.registerAction("citation", this.citation.bind(this));
		this.registerAction("help",this.helper.bind(this));
	}


	/* COMMANDES */
	
	hello(params)
	{
		if(params == '')
			app.sendMessage("Salut, je suis le meilleur compagnon !", this.name);
	}

	helper(params)
	{
		if (params == '') {
			app.sendMessage("Mes commandes sont : <ul><strong> <li>hello : pour vous saluer</li> <li>films : pour vous donner la liste des 10 films les plus populaires sur IMDb </li> <li>citation : pour vous donner mes citations préférées de Star wars </li> </strong></ul>",this.name)
		}
	}
	films(params)
	{
		const movie = new IMDb();

		movie.get(params).then(data => {
			app.sendMessage(
				movie.formatMessage(data),
				this.name
			);
		});
	}

	citation(params)
	{
		const citations = ["Que la force soit avec toi !","N'essaie pas! Fais-le ou ne le fais pas! Il n'y a pas d'essai.","La peur est le chemin vers le côté obscur: la peur mène à la colère, la colère mène à la haine, la haine… mène à la souffrance.","Luke, je suis ton père.","Allez Chico on met la gomme!","Plutôt embrasser un Wookie.","Personne par la guerre, ne devient grand.","Au secours Obi-Wan Kenobi, vous êtes mon seul espoir.","C'est donc ainsi que s'achève la liberté, sous un tonnerre d'applaudissements.","Nous étions comme des frères. Je t'aimais Anakin!","Chewie, on est à la maison","Ce n’est pas parce que tu parles que tu es intelligent.","Vous êtes venus dans cette casserole ? Mmmh, vous êtes plus courageux que je ne le pensais.","Vous voulez peut-être que je descende pour pousser ?","Il y en a toujours un pour manger l’autre !","La prophétie voulait que tu détruises les Sith pas que tu deviennes comme eux !","Exécutez l’ordre 66.","J’ai les puces à l’air ! Oh, juste ciel !","Vous avez encore beaucoup de choses à apprendre sur les femmes !","Il ne faut jamais sous-estimer un droïde."];
		if (params == '') {
			app.sendMessage(citations[Math.floor(Math.random()*citations.length)], this.name);
		}
	}
}
