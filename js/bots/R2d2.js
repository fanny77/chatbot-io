class R2D2 extends Bot
{

	constructor()
	{
		super("R2D2", "Le robot le plus intelligent de la galaxie !");
		this.registerAction("hello", this.hello.bind(this));
		this.registerAction("meteo", this.meteo.bind(this));
		this.registerAction("help",this.helper.bind(this));
		this.registerAction("infos",this.infos.bind(this));

	}

	/* COMMANDES */
	
	hello(params)
	{
		if(params == '')
			app.sendMessage("Bonjour, je m'appelle R2D2", this.name);
	}

	helper(params)
	{
		if (params == '') {
			app.sendMessage("Mes commandes sont : <ul><strong> <li>hello : pour vous saluer</li> <li>meteo &lt;Ville&gt; : pour vous donner la Météo en temps réel de la ville souhaitée </li></strong> <li><strong>infos [planete] [acteur] [type] [taille] : Je vous donne les informations sur moi que vous me demander</strong></li></ul>",this.name)
		}
	}
	meteo(params)
	{
		const weather = new Weather();

		weather.get(params).then(data => {
			app.sendMessage(
				weather.formatMessage(data),
				this.name
			);
		});
	}

	infos(params)
	{
		const requestParams = params.split(' ');
		let message = "";
		
		for (let i = 0; i < requestParams.length; i++) {
			switch (requestParams[i]) {
				case "planete":
					message = message + "Je suis originaire de Naboo <br>";
					break;
				case "acteur":
					message = message + "Je suis interprété par Kenny Baker et Jimmy Vee <br>";
					break;
				case "type":
					message = message + "Je suis un droïde astromécano <br>";
					break;
				case "taille":
					message = message + "Je fais 96cm <br>";
					break;
				}
		}

		if (message != "") {
			app.sendMessage(message, this.name);
		}
	}



}
